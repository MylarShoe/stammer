#!/usr/bin/python
# Sort, Transcode, And Media Muxer
#Windows version 1.0

import os
import sys
import time

def globalVariables():
	global audio_dir
	global hd_aud_dir
	global sd_aud_dir
	global complete_dir
	global source_aud_dir
	global video_dir
	global hd1080_dir
	global hd720_dir
	global sd360_dir
	global out_dir

	audio_dir = cwd + "audio\\"
	hd_aud_dir = audio_dir + "320k\\"
	sd_aud_dir = audio_dir + "256k\\"
	complete_dir = audio_dir + "completed\\"
	source_aud_dir = audio_dir + "Source\\"
	video_dir = cwd + "video\\"
	hd1080_dir = video_dir + "1080\\"
	hd720_dir = video_dir + "720\\"
	sd360_dir = video_dir + "360\\"
	out_dir = cwd + "output\\"


def folderCreate():
	if not os.path.exists(audio_dir):
		os.makedirs(audio_dir)
		logFile.write("\nCreated folder: " + audio_dir)
		os.makedirs(source_aud_dir)
		logFile.write("\nCreated folder: " + source_aud_dir)
		os.makedirs(hd_aud_dir)
		logFile.write("\nCreated folder: " + hd_aud_dir)
		os.makedirs(sd_aud_dir)
		logFile.write("\nCreated folder: " + sd_aud_dir)
		os.makedirs(complete_dir)
		logFile.write("\nCreated folder: " + complete_dir)
		
	if not os.path.exists(source_aud_dir):
		os.makedirs(source_aud_dir)
		logFile.write("\nCreated folder: " + source_aud_dir)
		
	if not os.path.exists(hd_aud_dir):
		os.makedirs(hd_aud_dir)
		logFile.write("\nCreated folder: " + hd_aud_dir)
		
	if not os.path.exists(sd_aud_dir):
		os.makedirs(sd_aud_dir)
		logFile.write("\nCreated folder: " + sd_aud_dir)
		
	if not os.path.exists(complete_dir):
		os.makedirs(complete_dir)
		logFile.write("\nCreated folder: " + complete_dir)
		
	if not os.path.exists(video_dir):
		os.makedirs(video_dir)
		logFile.write("\nCreated folder: " + video_dir)
		os.makedirs(hd1080_dir)
		logFile.write("\nCreated folder: " + hd1080_dir)
		os.makedirs(hd720_dir)
		logFile.write("\nCreated folder: " + hd720_dir)
		os.makedirs(sd360_dir)
		logFile.write("\nCreated folder: " + sd360_dir)
		
	if not os.path.exists(hd1080_dir):
		os.makedirs(hd1080_dir)
		logFile.write("\nCreated folder: " + hd1080_dir)
		
	if not os.path.exists(hd720_dir):
		os.makedirs(hd720_dir)
		logFile.write("\nCreated folder: " + hd720_dir)
		
	if not os.path.exists(sd360_dir):
		os.makedirs(sd360_dir)
		logFile.write("\nCreated folder: " + sd360_dir)
		
	if not os.path.exists(out_dir):
		os.makedirs(out_dir)
		logFile.write("\nCreated folder: " + out_dir)

def sorter():
	file_count = len([item for item in os.listdir(cwd) if os.path.isfile(os.path.join(cwd, item))])
	if file_count < 1:
		logFile.write("\nNo media files found")
		return
	while True:
		for filename in os.listdir(cwd):
			pair = os.path.splitext(filename)
			name = pair[0]
			ext = pair[1].lower()

			if ext == ".wav":
				os.system("ffmpeg -i " + cwd + filename + " -c:a libvo_aacenc -b:a 320k " + hd_aud_dir + name + ".aac")
				logFile.write("\nConverted: " + filename)
				logFile.write("\nffmpeg -i " + cwd + filename + " -c:a libvo_aacenc -b:a 320k " + hd_aud_dir + name + ".aac")
				os.system("ffmpeg -i " + cwd +  filename + " -c:a libvo_aacenc -b:a 128k -ac 1 " + sd_aud_dir + name + ".aac")
				logFile.write("\nffmpeg -i " + cwd + filename + " -c:a libvo_aacenc -b:a 128k -ac 1 " + sd_aud_dir + name + ".aac")
				os.system("move " + cwd + filename + " " + source_aud_dir + filename)
				logFile.write("\nmove " + cwd + filename + " " + source_aud_dir + filename)
			
			if ext == ".aac":
				os.system("ffmpeg -i " + filename + " -c:a libvo_aacenc -b:a 256k -ac 1 " + sd_aud_dir + name + ".aac")
				logFile.write("\nConverting: " + filename)
				logFile.write("\nffmpeg -i " + filename + " -c:a libvo_aacenc -b:a 256k -ac 1 " + sd_aud_dir + name + ".aac")
				os.system("move " + filename + " " + hd_aud_dir + filename)
				logFile.write("\nmove " + filename + " " + hd_aud_dir + filename)
				
			if ext == ".mp4":
				resolution = name[-4:]
				if resolution == "080p":
					os.system("move " + cwd + filename + " " + hd1080_dir + filename)
					logFile.write("\nmove " + cwd + filename + " " + hd1080_dir + filename)
				if resolution == "720p":
					os.system("move " + cwd + filename + " " + hd720_dir + filename)
					logFile.write("\nmove " + cwd + filename + " " + hd720_dir + filename)
				if resolution == "360p":
					os.system("move " + cwd + filename + " " + sd360_dir + filename)
					logFile.write("\nmove " + cwd + filename + " " + sd360_dir + filename)
		file_count = file_count - 1
		if file_count == 1:
			break
	
def muxExists(filename):
	for file in os.listdir(out_dir):
		if file == filename:
			return 1
	return 0
	
def vidExists(filename, res_dir):
	for files in os.listdir(res_dir):
		if files == filename:
			return 1
	return 0
	
def corename(filename):
	pair = os.path.splitext(filename)
	name = pair[0]
	return name[:-4]
	
def langcode(filename):
	pair = os.path.splitext(filename)
	name = pair[0]
	return name[-3:]
	
def muxer():
	for files in os.listdir(hd_aud_dir):
		basename = corename(files)
		language = langcode(files)
		base1080 = basename + "-1080p.mp4"
		base720 = basename + "-720p.mp4"
		mux1080 = basename + "-1080p-" + language + ".mp4"
		mux720 = basename + "-720p-" + language + ".mp4"
		
		if muxExists(mux1080) == 1:
			logFile.write("\n" + mux1080 + " mux already exists.")
		else:
			if vidExists(base1080, hd1080_dir) == 0:
				logFile.write("\n1080 video resolution not found")
			else:
				os.system("ffmpeg -i " + hd_aud_dir + files + " -i " + hd1080_dir + base1080 + " -c:v copy -strict -2 " + out_dir + mux1080)
				logFile.write("\nMuxing: " + files + " and " + mux1080)
				logFile.write("\nffmpeg -i " + hd_aud_dir + files + " -i " + hd1080_dir + base1080 + " -c:v copy -strict -2 " + out_dir + mux1080)
	
		if muxExists(mux720) == 1:
			logFile.write("\n" + mux720 + " mux already exists.")
		else:
			if vidExists(base720, hd720_dir) == 0:
				logFile.write("\n720 video resolution not found")
			else:
				os.system("ffmpeg -i " + hd_aud_dir + files + " -i " + hd720_dir + base720 + " -c:v copy -strict -2 " + out_dir + mux720)
				logFile.write("\nMuxing: " + files + " and " + mux720)
				logFile.write("\nffmpeg -i " + hd_aud_dir + files + " -i " + hd720_dir + base720 + " -c:v copy -strict -2 " + out_dir + mux720)
				os.system("move " + hd_aud_dir + files + " " + complete_dir + files)
				logFile.write("\nmove " + hd_aud_dir + files + " " + complete_dir + files)
			
	for files in os.listdir(sd_aud_dir):
		basename = corename(files)
		language = langcode(files)
		source_name = files[:-4]
		base360 = basename + "-360p.mp4"
		mux360 = basename + "-360p-" + language + ".mp4"
		
		if muxExists(mux360) == 1:
			logFile.write("\n" + mux360 + " mux already exists.")
		else:
			if vidExists(base360, sd360_dir) == 0:
				logFile.write("\n360 video resolution not found")
			else:
				os.system("ffmpeg -i " + sd_aud_dir + files + " -i " + sd360_dir + base360 + " -c:v copy -strict -2 " + out_dir + mux360)
				logFile.write("\nMuxing: " + files + " and " + mux360)
				logFile.write("\nffmpeg -i " + sd_aud_dir + files + " -i " + sd360_dir + base360 + " -c:v copy -strict -2 " + out_dir + mux360)
				os.system("move " + sd_aud_dir + files + " " + complete_dir + source_name + "-sd.aac")
				logFile.write("\nmove " + sd_aud_dir + files + " " + complete_dir + source_name + "-sd.aac")

def logOutput():
	if not os.path.exists(os.getcwd() + "\\logfiles\\"):
		os.makedirs(os.getcwd() + "\\logfiles\\")
	datetime = os.getcwd() + "\\logfiles\\" + time.strftime("%Y_%m_%d_%H_%M_%S_logfile.txt")
	global logFile
	logFile = open(datetime, 'w')
	logFile.write("\nSTAMmer started on " + time.strftime("%c") + " using " + argv[1] + " for parameters, and " + cwd + " for source media.")

def setCWD(argv, argc):
	global cwd
	if argc == 3:
		cwd = argv[2] + "\\"
	else:
		cwd = os.getcwd() + "\\"

def OptionParser(argv):
	argc = len(argv)
	setCWD(argv, argc)
	globalVariables()
	logOutput()
	if argc == 1:
		folderCreate()
		sorter()
		muxer()
	else:
		options = argv[1]
		if options[0] != "-":
			logFile.write("Invalid Parameters")
		else:
			if options.find("f") != -1:
				folderCreate()
			if options.find("s") != -1:
				sorter()
			if options.find("m") != -1:
				muxer()

argv = sys.argv

OptionParser(argv)
logFile.close()