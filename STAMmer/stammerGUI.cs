﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace STAMmer
{
	public partial class stammerGUI : Form
	{
		public stammerGUI()
		{
			InitializeComponent();

			this.SourceFolder.DragDrop += new System.Windows.Forms.DragEventHandler(this.SourceFolder_DragDrop);
			this.SourceFolder.DragEnter += new System.Windows.Forms.DragEventHandler(this.SourceFolder_DragEnter);
		}

		private void selectAll_Click(object sender, EventArgs e)
		{
			mux.Checked = true;
			sort.Checked = true;
			folders.Checked = true;
		}

		private void uncheckAll_Click(object sender, EventArgs e)
		{
			mux.Checked = false;
			sort.Checked = false;
			folders.Checked = false;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog destFolderSelectDialog = new FolderBrowserDialog();
			destFolderSelectDialog.ShowDialog();
			string selected = destFolderSelectDialog.SelectedPath;

			SourceFolder.Items.Clear();
			SourceFolder.Items.Add(selected);
		}

		private void SourceFolder_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.All;
			else
				e.Effect = DragDropEffects.None;
		}

		private void SourceFolder_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
			int i;
			for (i = 0; i < s.Length; i++)
			{
				if (Directory.Exists(s[i]))
				{
					SourceFolder.Items.Clear();
					SourceFolder.Items.Add(s[i]);
				}
				else
				{
					if (SourceFolder.Items.Contains(s[i]) == true)
					{
						MessageBox.Show("Only folders can be dropped here.", "Error");
						continue;
					}
					else
					{

					}
				}
			}
		}

		private void start_Click(object sender, EventArgs e)
		{
			string parameters = "-";
			if (folders.Checked == true)
			{
				parameters += "f";
			}
			if (sort.Checked == true)
			{
				parameters += "s";
			}
			if (mux.Checked == true)
			{
				parameters += "m";
			}
			if (SourceFolder.Items.Count > 0)
			{
				parameters += " " + SourceFolder.Items[0];
			}
			Process process = new Process();
			// Configure the process using the StartInfo properties.
			process.StartInfo.FileName = "cmd.exe";
			string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string rendererPath = Path.Combine(path, "renderer.exe");
			string rendererParams = parameters;
			process.StartInfo.UseShellExecute = true;
			process.StartInfo.Arguments = "/k " + rendererPath + " " + rendererParams;
			//process.StartInfo.CreateNoWindow = false;
			//process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
			process.Start();
			//process.WaitForExit();// Waits here for the process to exit.
		}
	}
}
