﻿namespace STAMmer
{
	partial class stammerGUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(stammerGUI));
			this.folders = new System.Windows.Forms.CheckBox();
			this.sort = new System.Windows.Forms.CheckBox();
			this.mux = new System.Windows.Forms.CheckBox();
			this.selectAll = new System.Windows.Forms.Button();
			this.uncheckAll = new System.Windows.Forms.Button();
			this.sourceSelect = new System.Windows.Forms.Button();
			this.start = new System.Windows.Forms.Button();
			this.SourceFolder = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// folders
			// 
			this.folders.AutoSize = true;
			this.folders.Checked = true;
			this.folders.CheckState = System.Windows.Forms.CheckState.Checked;
			this.folders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.folders.Location = new System.Drawing.Point(22, 45);
			this.folders.Name = "folders";
			this.folders.Size = new System.Drawing.Size(313, 35);
			this.folders.TabIndex = 0;
			this.folders.Text = "Create Sorting Folders";
			this.folders.UseVisualStyleBackColor = true;
			// 
			// sort
			// 
			this.sort.AutoSize = true;
			this.sort.Checked = true;
			this.sort.CheckState = System.Windows.Forms.CheckState.Checked;
			this.sort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.sort.Location = new System.Drawing.Point(473, 45);
			this.sort.Name = "sort";
			this.sort.Size = new System.Drawing.Size(278, 35);
			this.sort.TabIndex = 1;
			this.sort.Text = "Sort and Transcode";
			this.sort.UseVisualStyleBackColor = true;
			// 
			// mux
			// 
			this.mux.AutoSize = true;
			this.mux.Checked = true;
			this.mux.CheckState = System.Windows.Forms.CheckState.Checked;
			this.mux.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.mux.Location = new System.Drawing.Point(881, 45);
			this.mux.Name = "mux";
			this.mux.Size = new System.Drawing.Size(90, 35);
			this.mux.TabIndex = 2;
			this.mux.Text = "Mux";
			this.mux.UseVisualStyleBackColor = true;
			// 
			// selectAll
			// 
			this.selectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.selectAll.Location = new System.Drawing.Point(202, 104);
			this.selectAll.Name = "selectAll";
			this.selectAll.Size = new System.Drawing.Size(247, 46);
			this.selectAll.TabIndex = 3;
			this.selectAll.Text = "Select All";
			this.selectAll.UseVisualStyleBackColor = true;
			this.selectAll.Click += new System.EventHandler(this.selectAll_Click);
			// 
			// uncheckAll
			// 
			this.uncheckAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.uncheckAll.Location = new System.Drawing.Point(522, 104);
			this.uncheckAll.Name = "uncheckAll";
			this.uncheckAll.Size = new System.Drawing.Size(247, 46);
			this.uncheckAll.TabIndex = 4;
			this.uncheckAll.Text = "Uncheck All";
			this.uncheckAll.UseVisualStyleBackColor = true;
			this.uncheckAll.Click += new System.EventHandler(this.uncheckAll_Click);
			// 
			// sourceSelect
			// 
			this.sourceSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.047121F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.sourceSelect.Location = new System.Drawing.Point(789, 189);
			this.sourceSelect.Name = "sourceSelect";
			this.sourceSelect.Size = new System.Drawing.Size(200, 50);
			this.sourceSelect.TabIndex = 6;
			this.sourceSelect.Text = "Select Source";
			this.sourceSelect.UseVisualStyleBackColor = true;
			this.sourceSelect.Click += new System.EventHandler(this.button1_Click);
			// 
			// start
			// 
			this.start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.17801F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.start.Location = new System.Drawing.Point(370, 278);
			this.start.Name = "start";
			this.start.Size = new System.Drawing.Size(250, 72);
			this.start.TabIndex = 7;
			this.start.Text = "Start";
			this.start.UseVisualStyleBackColor = true;
			this.start.Click += new System.EventHandler(this.start_Click);
			// 
			// SourceFolder
			// 
			this.SourceFolder.AllowDrop = true;
			this.SourceFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.93194F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SourceFolder.FormattingEnabled = true;
			this.SourceFolder.ItemHeight = 33;
			this.SourceFolder.Location = new System.Drawing.Point(22, 189);
			this.SourceFolder.Name = "SourceFolder";
			this.SourceFolder.Size = new System.Drawing.Size(747, 70);
			this.SourceFolder.TabIndex = 8;
			// 
			// stammerGUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(1001, 390);
			this.Controls.Add(this.SourceFolder);
			this.Controls.Add(this.start);
			this.Controls.Add(this.sourceSelect);
			this.Controls.Add(this.uncheckAll);
			this.Controls.Add(this.selectAll);
			this.Controls.Add(this.mux);
			this.Controls.Add(this.sort);
			this.Controls.Add(this.folders);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = new System.Drawing.Point(1027, 461);
			this.MaximumSize = new System.Drawing.Size(1027, 461);
			this.Name = "stammerGUI";
			this.Text = "STAMmer";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox folders;
		private System.Windows.Forms.CheckBox sort;
		private System.Windows.Forms.CheckBox mux;
		private System.Windows.Forms.Button selectAll;
		private System.Windows.Forms.Button uncheckAll;
		private System.Windows.Forms.Button sourceSelect;
		private System.Windows.Forms.Button start;
		private System.Windows.Forms.ListBox SourceFolder;
	}
}

